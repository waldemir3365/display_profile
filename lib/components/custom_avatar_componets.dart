import 'package:flutter/material.dart';

class CustomCircleAvatar extends StatelessWidget {
  const CustomCircleAvatar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      alignment: Alignment.center,
      children: [
        Container(
          height: 100,
          width: 100,
          decoration: const BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(80)),
              color: Colors.white,
          ),
        ),
        const CircleAvatar(
          radius:45,
          backgroundImage: NetworkImage('https://www.petlove.com.br/images/breeds/193436/profile/original/beagle-p.jpg?1532538271'),
        )
      ],
    );
  }
}

import 'package:flutter/material.dart';

class RowCustom extends StatelessWidget {
  final String text1;
  final String text2;
  final String text3;
  final bool isNumber;
  const RowCustom({Key? key, required this.text1, required this.text2, required this.text3, required this.isNumber}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(10),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Text(text1,
            style: TextStyle(
                color: isNumber ? Colors.brown: Colors.black45,
                fontSize: isNumber? 18: 15,
                fontWeight:isNumber ? FontWeight.bold:FontWeight.normal,
            ),
          ),
          Text(text2,
            style: TextStyle(
              color: isNumber ? Colors.brown: Colors.black45,
              fontSize: isNumber? 18: 15,
              fontWeight:isNumber ? FontWeight.bold:FontWeight.normal,
            ),
          ),
          Text(text3,
            style: TextStyle(
              color: isNumber ? Colors.brown: Colors.black45,
              fontSize: isNumber? 18: 15,
              fontWeight:isNumber ? FontWeight.bold:FontWeight.normal,
            ),
          ),
        ],
      ),
    );
  }
}

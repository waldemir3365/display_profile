import 'package:flutter/material.dart';

class MyCustomClipper extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path = Path();
    path.lineTo(0, size.height-60);
    path.quadraticBezierTo(size.width/6, size.height-65, size.width/2, size.height-20);
    path.quadraticBezierTo(3/4*size.width,size.height,size.width, size.height-45);
    path.lineTo(size.width,0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper oldClipper) {
    return true;
  }
}
import 'package:flutter/material.dart';

class CustomDataPersonal extends StatelessWidget {
  final IconData icon;
  final String text;

  const CustomDataPersonal({Key? key, required this.icon, required this.text})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(left: 40),
      child: Padding(
        padding: EdgeInsets.symmetric(vertical: 16, horizontal: 20),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Icon(icon,color: Colors.brown,size: 26,),
            const SizedBox(width: 20),
            Text(text,style: TextStyle(color: Colors.brown.shade200,fontSize:15),),
          ],
        ),
      ),
    );
  }
}
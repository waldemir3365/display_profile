import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:untitled/components/custom_avatar_componets.dart';
import 'package:untitled/components/custom_data_personal.dart';
import 'package:untitled/components/row_custom_.dart';

import 'components/utils/my_clipper.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Stack(
            children: [
              Container(
                width: MediaQuery.of(context).size.width,
                child: ClipPath(
                  clipper: MyCustomClipper(),
                  child: Image.network('https://cptstatic.s3.amazonaws.com/imagens/enviadas/materias/materia10369/raca-beagle-2-cursos-cpt.jpg',
                    height:300,
                    width:double.infinity,
                    fit: BoxFit.fill,
                  ),
                ),
              ),
             const Positioned(
               top: 190,
                 left: 20,
                 child: CustomCircleAvatar()
             ),
            ],
          ),
          lineFrinds(),
          linePersonal(),
        ],
      )
    );
  }

  Widget lineFrinds(){
    return Expanded(
      flex: 2,
      child: Column(
        children:[
          RowCustom(text1:'3057', text2:'274', text3:'52',isNumber: true),
          RowCustom(text1:' Followers', text2:' Following', text3:'Collection',isNumber: false),
          dividerRow(),
        ],
      ),
    );
  }
  Widget linePersonal(){
    return Expanded(
      flex: 5,
      child: Column(
        children:const  [
          CustomDataPersonal(icon: Icons.email, text:'waldemir.gomes@gmail.com'),
          CustomDataPersonal(icon: Icons.call, text:'+82 95 5808 2654'),
          CustomDataPersonal(icon: Icons.wifi_tethering_sharp, text:'www.lauleith.com'),
        ],
      ),
    );
  }
  Widget dividerRow(){
    return const SizedBox(
      width: 340,
      child: Divider(color: Colors.grey,height: 2,),
    );
  }
}
